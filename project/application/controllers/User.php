<?php
class user extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $data['users'] = $this->user_model->get_users();

        $data['name'] = 'User\'s archive';

        $this->load->view('templates/header', $data);
        $this->load->view('user/users', $data);
        $this->load->view('templates/footer');
    }

    public function view($email = NULL)
    {



        $data['user_item'] = $this->user_model->get_users($email);

        if (empty($data['user_item']))
        {
            show_404();
        }

        $data['name'] = $data['user_item']['name'];

        $this->load->view('templates/header', $data);
        $this->load->view('user/view', $data);
        $this->load->view('templates/footer');
    }










public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');


        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header');
            $this->load->view('user/create');
            $this->load->view('templates/footer');

        }
        else
        {
            $this->user_model->set_user();
//            $this->load->view('user/success');
        }
    }



    public function validate()
    {

            $this->load->view('user/validate');

    }

}