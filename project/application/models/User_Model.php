<?php
class User_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_users($email = FALSE)
    {
        if ($email === FALSE)
        {
            $query = $this->db->get('users');
            return $query->result_array();
        }

        $query = $this->db->get_where('users', array('email' => $email));
        return $query->row_array();
    }





    public function set_user()
    {



        $this->load->helper('url');

        $this->load->library('form_validation');

        $query = null; //emptying in case

        $email = $_POST['email'];

        $query = $this->db->get_where('users', array(
            'email' => $email
        ));

        $count = $query->num_rows();

        if ($count === 0) {
            $data_user = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'age' => $this->input->post('age')
            );
            $data_roles = array(
                'designer' => $this->input->post('designer'),
                'director' => $this->input->post('director'),
                'agent' => $this->input->post('agent')
            );
            $this->db->insert('users', $data_user);
            $this->db->insert('roles', $data_roles);
            $insert_id = $this->db->insert_id();
            $data_user_roles = array(
                'user_id' => $insert_id,
                'role_id' => $insert_id
            );
            $this->db->insert('users_roles', $data_user_roles);
            $this->load->view('user/success');
//            redirect($_SERVER['HTTP_REFERER']);
//            $message = "Success";
//            echo "<script type='text/javascript'>alert('$message');</script>";

        }else{
            // does not work
            $this->form_validation->set_message(
                'Email address already exist.'
            );
            $message = "Email address already exist";
            echo "<script type='text/javascript'>alert('$message');</script>";
            $this->load->view('user/create');

        }



//        return $this->db->insert('users', $data);
    }
}