<html>
<head>
    <title>yopta</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <style>
        body{
            margin:0;
            color:rgba(255, 255, 255, 0.5);
            background:#fff;
            font:600 16px/18px 'Open Sans',sans-serif;
            padding-top: 50px;
        }


        .login-wrap{
            border-radius: 10px;
            width:400px;
            margin:auto;
            max-width:450px;
            min-height:760px;
            position:relative;
            box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
        }
        .login-html{
            border-radius: 10px;
            width:100%;
            height:100%;
            position:absolute;
            padding:90px 70px 50px 70px;
            background:rgba(146, 31, 96, 0.9);
        }
        .login-html .sign-up,
        .login-form .group .check{
            display:none;
        }
        .login-html .tab,
        .login-form .group .label,
        .login-form .group .button{
            text-transform:uppercase;
        }

        .login-form .group{
            margin-bottom:15px;
        }
        .login-form .group .label,
        .login-form .group .input,
        .login-form .group .button{
            width:100%;
            color:#fff;
            display:block;
        }
        .login-form .group .input,
        .login-form .group .button{
            border:none;
            padding:15px 25px;
            border-radius:10px;
            background:rgba(255,255,255,.1);
        }

        input, select{
            outline: none;
        }

        .button:focus{
            outline: none;
            background: #d8054c;
        }

        .login-form .group input[data-type="password"]{
            text-security:circle;
            -webkit-text-security:circle;
        }
        .login-form .group .label{
            color:rgba(255, 255, 255, 0.5);
            font-size:12px;
        }
        .login-form .group .button{
            background:#ff0056;
        }

    </style>
</head>
<body>

